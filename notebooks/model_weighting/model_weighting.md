---
title: "Model weighting"
author: "Justin Brantley"
date: "April 23, 2022"
output:
  rmarkdown::html_vignette:
    keep_md: true
vignette: >
  %\VignetteIndexEntry{Model weighting}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---





# Weighting

1. User-tuned observations and model-tuned observations are weighted 80:20
2. Weights are decayed over time with a half-life of four years
3. Model-tuned observations are further decayed with a half-life of four weeks

<img src="model_weighting_files/figure-html/weighting-1.png" width="100%" />

4. And successive identical observations are rapidly decayed with a half-life of
   five minutes

<img src="model_weighting_files/figure-html/weighting_half_life-1.png" width="100%" />

In total, 174,299 observations were collected
from 7 October 2018 to 17 March 2019.
After weighting, the sample size is effectively reduced to 41,989.68
observations.

# Exploratory plots

<img src="model_weighting_files/figure-html/exploratory_plots-1.png" width="100%" /><img src="model_weighting_files/figure-html/exploratory_plots-2.png" width="100%" /><img src="model_weighting_files/figure-html/exploratory_plots-3.png" width="100%" /><img src="model_weighting_files/figure-html/exploratory_plots-4.png" width="100%" /><img src="model_weighting_files/figure-html/exploratory_plots-5.png" width="100%" /><img src="model_weighting_files/figure-html/exploratory_plots-6.png" width="100%" /><img src="model_weighting_files/figure-html/exploratory_plots-7.png" width="100%" />


<img src="model_weighting_files/figure-html/solar_elevation-1.png" width="100%" />
<img src="model_weighting_files/figure-html/solar_azimuth-1.png" width="100%" />
<img src="model_weighting_files/figure-html/seasonal_variation-1.png" width="100%" />
<img src="model_weighting_files/figure-html/diurnal_variation-1.png" width="100%" />
