# <a href="https://fascinatingfingers.gitlab.io/zeitgeber" target="_blank">Open zeitgebeR documentation site</a>

# zeitgebeR <a href="https://fascinatingfingers.gitlab.io/zeitgeber"></a>

<!-- badges: start -->
[![pipeline status](https://gitlab.com/fascinatingfingers/zeitgeber/badges/main/pipeline.svg)](https://gitlab.com/fascinatingfingers/zeitgeber/-/pipelines)
[![coverage report](https://gitlab.com/fascinatingfingers/zeitgeber/badges/main/coverage.svg)](https://gitlab.com/fascinatingfingers/zeitgeber)
<!-- badges: end -->
